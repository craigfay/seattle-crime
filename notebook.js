// URL: https://beta.observablehq.com/d/726f4f51a09ca3c3
// Title: Untitled
// Author: Craig-Benjamin (@cbfay)
// Version: 177
// Runtime version: 1

const m0 = {
  id: "726f4f51a09ca3c3@177",
  variables: [
    {
      name: "d3",
      inputs: ["require"],
      value: (function(require){return(
require('d3')
)})
    },
    {
      name: "parseTime",
      inputs: ["d3"],
      value: (function(d3){return(
d3.timeParse('%m/%d/%Y%H%M')
)})
    },
    {
      name: "rawData",
      inputs: ["d3","parseTime"],
      value: (function(d3,parseTime){return(
d3.csv('https://data.seattle.gov/api/views/4fs7-3vj5/rows.csv?accessType=DOWNLOAD', d => {
  return {
    timeOccurred: parseTime(d['Occurred Date'] + d['Occurred Time']),
    timeReported: parseTime(d['Reported Date'] + d['Reported Time']),
    neighborhood: d['Neighborhood'],
    descriptions: d['Primary Offense Description']
  }
})
)})
    },
    {
      name: "data",
      inputs: ["rawData"],
      value: (function(rawData){return(
rawData.filter(d => d.timeOccurred != null)
  .filter(d => d.timeOccurred.getFullYear() == '2015')
)})
    },
    {
      name: "dateRange",
      inputs: ["d3","data"],
      value: (function(d3,data){return(
d3.extent(data, d => d.timeOccurred)
)})
    },
    {
      name: "countYear",
      inputs: ["data"],
      value: (function(data){return(
(year) => (data.filter(d => d.timeOccurred != null && d.timeOccurred.getFullYear() == year)).length
)})
    },
    {
      name: "width",
      value: (function(){return(
1000
)})
    },
    {
      name: "height",
      value: (function(){return(
600
)})
    },
    {
      name: "margin",
      value: (function(){return(
{top: 30, right: 30, bottom: 30, left: 30}
)})
    },
    {
      name: "x",
      inputs: ["d3","dateRange","margin","width"],
      value: (function(d3,dateRange,margin,width){return(
d3.scaleTime()
  .domain(dateRange)
  .range([margin.left, width - margin.right])
)})
    },
    {
      name: "y",
      inputs: ["d3","height","margin"],
      value: (function(d3,height,margin){return(
d3.scaleLinear()
  .domain(0, 400)
  .range([height - margin.bottom, margin.top])
)})
    },
    {
      inputs: ["d3","DOM","width","height","x","margin","y"],
      value: (function(d3,DOM,width,height,x,margin,y)
{
  let svg = d3.select(DOM.svg(width, height))
  
  // Bottom Axis Scale
  svg.append('g')
    .call(d3.axisBottom(x))
    .attr('transform', `translate(0, ${height - margin.bottom})`)
   
  // Left Axis Scale
  svg.append('g')
    .call(d3.axisLeft(y))
    .attr('transform', `translate(${margin.left}, 0)`)
  
  
  
  return svg.node()
  
}
)
    }
  ]
};

const notebook = {
  id: "726f4f51a09ca3c3@177",
  modules: [m0]
};

export default notebook;