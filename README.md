# Seattle Crime

An analysis of known crime in Seattle, WA between 2008 and 2018.
* URL: https://beta.observablehq.com/d/726f4f51a09ca3c3
* Dataset: https://catalog.data.gov/dataset/crime-data-76bd0